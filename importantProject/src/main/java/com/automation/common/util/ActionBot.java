package com.automation.common.util;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ActionBot {

	/*  We can keep all the selenium actions in this class currently I have implemented few methods as per my requirement
	 * 
	 * 
	 */
	
	public static void click(WebDriver driver, WebElement ele) {
		(new WebDriverWait(driver, 30)).until(ExpectedConditions.visibilityOf(ele));
		ele.click();
	}

	public static void type(WebDriver driver, WebElement ele, String value) {
		(new WebDriverWait(driver, 30)).until(ExpectedConditions.visibilityOf(ele));
		ele.click();
		ele.clear();
		ele.sendKeys(value);
	}

	public static String getText(WebDriver driver, WebElement ele) {
		(new WebDriverWait(driver, 30)).until(ExpectedConditions.visibilityOf(ele));
		return ele.getText();
	}

	public static void frameSwitchingByElement(WebDriver driver, WebElement ele) {
		driver.switchTo().frame(ele);
	}

	public static boolean isDisplayed(WebDriver driver, WebElement ele) {
		
		boolean bool=false;
		try {
			
			 bool = (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOf(ele)).isDisplayed();
			
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Retrying to find element*******************");
			try {
				 bool = (new WebDriverWait(driver, 60)).until(ExpectedConditions.visibilityOf(ele)).isDisplayed();
			} catch (Exception e2) {
				
				System.out.println("Element not founds");
			}
		}
		
		return bool;
	}
	
	public static void switchToDefaultContent(WebDriver driver){
		driver.switchTo().defaultContent();
	}
	
}
