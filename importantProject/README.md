﻿# Project Title

**MidTrans Website Automation:**
This automation framework is designed to purchase pillow. In this project we have two scenario, one is buying pillow with valid credit card and second one is purchasing with invalid card details. Hence one test should pass with success message and other pass with failed message.


# Prerequisites

Java8
Maven
Chrome Browser
Firefox Browser


## Running the tests

Got to the directory where we have kept the pom.xml and execute below command on cmd or terminal

    mvn clean package exec:java -Dbrowser=FF

## Findings

The provided website doesn't work on chrome 83. It stuck while doing payment. For you reference I have attached he screenshot in "Finding" folder.

